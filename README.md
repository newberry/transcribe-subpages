# Newberry Transcribe Sub-Pages

These two small pages were floating around, so I've put them into one repo; this repo will also (eventually) old the OOO page and any other one-offs that come up.

Both written in SvelteJS using Newberry Transcribe 2.x design language.  Originally done as discrete mini-projects, they've been combined in the /unified/ folder.

To build, and not ruin Transcribe: 

```pnpm run build && /bin/rm build/index* && rsync -avz build/* $CSRVR/public_html/transcribe```

Transcribathon will remain discrete because it's a whole thing.