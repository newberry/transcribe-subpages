import adapter from '@sveltejs/adapter-static';
import path from 'path'
import { vitePreprocess } from '@sveltejs/kit/vite';
const dev = process.argv.includes('dev');

/** @type {import('@sveltejs/kit').Config} */
const config = {
	preprocess: [vitePreprocess()],
	kit: {
		
		adapter: adapter({
			precompress: true
		}),
		paths: {
			base: dev ? '' : '/transcribe'
		},
		alias: {
			$src: path.resolve('./src'),
			$comps: path.resolve('./src/comps')
		}
	}
};

export default config;
