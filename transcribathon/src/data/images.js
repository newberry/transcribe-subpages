export const images = [
    {
        title: 'Taylor, Graham, 1851-1938',
        src: '2KXJ8ZS5R7NS8',
        height: 3600,
        width: 2365,
    },
    {
        title: 'Chicago Commons printed ephemera [excerpts], 1890s-1940s',
        src: '2KXJ8ZS5R7W4U',
        height: 1364,
        width: 2000
    },
    {
        title: 'Chicago Commons printed ephemera [excerpts], 1890s-1940s ',
        src: '2KXJ8ZS5R7D2U',
        height: 3600,
        width: 2410,
    },
    {
        title: 'Taylor, Graham, 1851-1938 ',
        src: '2KXJ8ZS5RDQBX',
        height: 3600,
        width: 2370,
    },
    {
        title: 'Taylor, Graham, 1851-1938 ',
        src: '2KXJ8ZS5R7IR7',
        height: 3600,
        width: 2507,
    }
]
export const unusedImages = [{
        title: 'NL11HJQU',
        link: 'https://collections.newberry.org/asset-management/2KXJ8ZS53NG_H',
        img: '2KXJ8ZS53NG_H',
        src: 'https://collections.newberry.org/IIIF3/Image/2KXJ8ZS53NG_H/full/max/0/default.jpg',
        thumb: 'https://collections.newberry.org/IIIF3/Image/2KXJ8ZS53NG_H/full/,300/0/default.jpg',
        coll: 'gallery',
        height: 1000,
        width: 1622
    },
    {
        title: 'NL11HJQT',
        link: 'https://collections.newberry.org/asset-management/2KXJ8ZS53NY_S',
        img: '2KXJ8ZS53NY_S',
        src: 'https://collections.newberry.org/IIIF3/Image/2KXJ8ZS53NY_S/full/max/0/default.jpg',
        thumb: 'https://collections.newberry.org/IIIF3/Image/2KXJ8ZS53NY_S/full/,300/0/default.jpg',
        coll: 'gallery',
        height: 1000,
        width: 1764
    },
    {
        title: 'NL11HJQO',
        link: 'https://collections.newberry.org/asset-management/2KXJ8ZS53NA_N',
        img: '2KXJ8ZS53NA_N',
        src: 'https://collections.newberry.org/IIIF3/Image/2KXJ8ZS53NA_N/full/max/0/default.jpg',
        thumb: 'https://collections.newberry.org/IIIF3/Image/2KXJ8ZS53NA_N/full/,300/0/default.jpg',
        coll: 'gallery',
        height: 1000,
        width: 781
    },
    {
        title: 'NL11HJQL',
        link: 'https://collections.newberry.org/asset-management/2KXJ8ZS5RD597',
        img: '2KXJ8ZS5RD597',
        src: 'https://collections.newberry.org/IIIF3/Image/2KXJ8ZS5RD597/full/max/0/default.jpg',
        thumb: 'https://collections.newberry.org/IIIF3/Image/2KXJ8ZS5RD597/full/,300/0/default.jpg',
        coll: 'gallery',
        height: 1000,
        width: 1537
    },
    {
        title: 'NL11HJPX',
        link: 'https://collections.newberry.org/asset-management/2KXJ8ZS5R7ENO',
        img: '2KXJ8ZS5R7ENO',
        src: 'https://collections.newberry.org/IIIF3/Image/2KXJ8ZS5R7ENO/full/max/0/default.jpg',
        thumb: 'https://collections.newberry.org/IIIF3/Image/2KXJ8ZS5R7ENO/full/,300/0/default.jpg',
        coll: 'gallery',
        height: 1000,
        width: 1524
    },
    {
        title: 'NL11HJPR',
        link: 'https://collections.newberry.org/asset-management/2KXJ8ZS5R7R7R',
        img: '2KXJ8ZS5R7R7R',
        src: 'https://collections.newberry.org/IIIF3/Image/2KXJ8ZS5R7R7R/full/max/0/default.jpg',
        thumb: 'https://collections.newberry.org/IIIF3/Image/2KXJ8ZS5R7R7R/full/,300/0/default.jpg',
        coll: 'gallery',
        height: 1000,
        width: 1499
    },
    {
        title: 'NL11HJNX',
        link: 'https://collections.newberry.org/asset-management/2KXJ8ZS5RD72I',
        img: '2KXJ8ZS5RD72I',
        src: 'https://collections.newberry.org/IIIF3/Image/2KXJ8ZS5RD72I/full/max/0/default.jpg',
        thumb: 'https://collections.newberry.org/IIIF3/Image/2KXJ8ZS5RD72I/full/,300/0/default.jpg',
        coll: 'gallery',
        height: 1000,
        width: 1792
    },
    {
        title: 'NL11HJNN',
        link: 'https://collections.newberry.org/asset-management/2KXJ8ZS5R7SEK',
        img: '2KXJ8ZS5R7SEK',
        src: 'https://collections.newberry.org/IIIF3/Image/2KXJ8ZS5R7SEK/full/max/0/default.jpg',
        thumb: 'https://collections.newberry.org/IIIF3/Image/2KXJ8ZS5R7SEK/full/,300/0/default.jpg',
        coll: 'gallery',
        height: 1000,
        width: 1631
    },
    {
        title: 'NL11HJN5',
        link: 'https://collections.newberry.org/asset-management/2KXJ8ZS5RAVIQ',
        img: '2KXJ8ZS5RAVIQ',
        src: 'https://collections.newberry.org/IIIF3/Image/2KXJ8ZS5RAVIQ/full/max/0/default.jpg',
        thumb: 'https://collections.newberry.org/IIIF3/Image/2KXJ8ZS5RAVIQ/full/,300/0/default.jpg',
        coll: 'gallery',
        height: 1000,
        width: 1881
    },
    {
        title: 'NL11HJN4',
        link: 'https://collections.newberry.org/asset-management/2KXJ8ZS5RA83W',
        img: '2KXJ8ZS5RA83W',
        src: 'https://collections.newberry.org/IIIF3/Image/2KXJ8ZS5RA83W/full/max/0/default.jpg',
        thumb: 'https://collections.newberry.org/IIIF3/Image/2KXJ8ZS5RA83W/full/,300/0/default.jpg',
        coll: 'gallery',
        height: 1000,
        width: 1791
    }
];