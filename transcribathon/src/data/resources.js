export const data = [
    {
        title: 'Graham Taylor digital collection (Newberry)',
        link: 'https://collections.newberry.org/C.aspx?VP3=DamView&KW_RID=2KXJ2046NPD&FR_=1&W=1536&H=754',
        img: '/gt-dig-coll.png',
        coll: 'external'
    },
    {
        title: 'Graham Taylor papers collection guide (Newberry)',
        link: 'https://archives.newberry.org/repositories/2/resources/266',
        viiif: 'https://collections.newberry.org/IIIF3/Image/2KXJ8ZS530A3Z/full/,300/0/default.jpg',
        img: 'https://collections.newberry.org/IIIF3/Image/2KXJ8ZS5RDGNP/full/,300/0/default.jpg',
        coll: 'external'
    },
    {
        title: 'Settlement Houses (Encyclopedia of Chicago)',
        link: 'http://www.encyclopedia.chicagohistory.org/pages/1135.html',
        img: '/settshot.png',
        coll: 'external'
    },
    {
        title: 'Our History (Chicago Commons)',
        link: "https://www.chicagocommons.org/about-2/our-history/#:~:text=Founded%20by%20Graham%20Taylor%20in,city's%20earliest%20kindergartens%20in%201897.",
        img: 'https://www.chicagocommons.org/wp-content/uploads/2017/03/1901.jpg ',
        coll: 'external'
    },
    {
        title: 'Teaching resource: The Jungle and the Community (Newberry)',
        extratitle: 'Workers and Reformers in Turn-of-the-Century Chicago (Newberry)',
        link: 'https://dcc.newberry.org/?p=14384',
        img: 'https://collections.newberry.org/IIIF3/Image/2KXJ8ZSG2FUS1/full/,300/0/default.jpg',
        coll: 'external'
    },
    {
        title: 'Transcribe guidelines (Newberry)',
        link: 'https://digital.newberry.org/transcribe/guidelines/',
        img: 'https://collections.newberry.org/IIIF3/Image/2KXJ8ZID4XO1/full/,300/0/default.jpg',
        coll: 'newberry'
    },
    {
        title: 'About our social activist collections (Newberry)',
        link: 'https://digital.newberry.org/transcribe/social-activism/',
        viiif:
            ['https://collections.newberry.org/IIIF3/Image/2KXJ8ZSG2JSQ9/full/,300/0/default.jpg','https://collections.newberry.org/IIIF3/Image/2KXJ8ZS530FV3/full/,300/0/default.jpg'],
        img: '/transcribe-sa-trim.png',
        coll: 'newberry'
    },
    {
        title: 'Sample transcription:',
        subtitle: 'Vanzetti letter to Debs regarding death sentence and appeal, 1927 (Newberry)',
        link: 'https://publications.newberry.org/transcribe/#/item/1578/page/138705',
        img: 'https://digital.newberry.org/transcribe/omeka/files/original/130b23f784f9fba174c25aa943032821.jpg ',
        coll: 'newberry'
    },
    {
        title: 'Featured transcriptions at Midwest Time Machine (Newberry)',
        link: 'https://digital.newberry.org/time-machine/',
        img: '/mtm-h.png',
        coll: 'newberry'
    },
    {
        title: 'Featured transcriptions at @DigitalNewberry (Twitter)',
        link: 'https://twitter.com/search?q=(%23otd)%20(from%3Adigitalnewberry)&src=typed_query&f=live',
        img: '/tweet.png',
        coll: 'newberry'
    }
];
