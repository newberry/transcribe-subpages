// import { BiggerPictureInstance } from 'bigger-picture'

let bp


export async function loadBp() {
    if (!bp) {
      const BiggerPicture = (await import('bigger-picture/svelte')).default;
    // const  BiggerPicture = await import('bigger-picture/svelte')
    bp = BiggerPicture({
      target: document.body,
    })
  }
  return bp
}
