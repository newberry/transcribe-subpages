// import { BiggerPictureInstance } from 'bigger-picture'

let bp

export async function loadBp() {
  if (!bp) {
    const { default: BiggerPicture } = await import('bigger-picture/svelte')
    bp = BiggerPicture({
      target: document.body,
    })
  }
  console.log("ok")
  return bp
}
